import logging

import discord
from core.date_utils import now_as_str
from core.sheet_store import SheetStore
from discord.ext import commands
from features.custom_permissions import has_dm_command_permission
from services.app_data import AppData

from .base_cog import BaseCog


class FileCog(BaseCog):
    """ Command group handling download / upload """
    def __init__(self, bot: commands.Bot, app_data: AppData, logger):
        self.bot = bot
        self.app_data = app_data
        self.logger = logger

    @commands.command()
    @has_dm_command_permission()
    async def download(self, ctx):
        await self.__download_storage(ctx.author)

    @commands.command()
    @has_dm_command_permission()
    async def upload(self, ctx):
        guild = await self.get_guild()
        if guild is None:
            logging.error('Cannot find the server')
            return
        await self.__update_storage(guild, ctx.author, ctx.message)

    async def __download_storage(self, author):
        updated_file = self.app_data.storage.save()
        await author.create_dm()
        await author.send('Update', file=discord.File(updated_file, 'up.xlsx'))
        updated_file.close()

    async def __update_storage(self, guild: discord.Guild, author,
                               msg: discord.Message):
        await author.create_dm()
        if len(msg.attachments) != 1:
            await self.send_and_log(logging.DEBUG, author,
                                    "Missing attachment")
            return

        config = self.get_config()
        data_source = msg.attachments[0]
        if data_source.filename != config.allowed_file or data_source.size > config.max_file_size:
            await self.send_and_log(logging.DEBUG, author,
                                    "File not supported or too large")
            return

        content = await data_source.read()
        try:
            self.app_data.storage = SheetStore.load_data_from_bytes(\
                content, config.task_sheet, config.worker_history_sheet)
            admin_channel = discord.utils.get(guild.channels,
                                              name=config.admin_room)
            formatted_date = config.upload_dir + now_as_str()
            self.app_data.storage.save_file(formatted_date)
            self.app_data.storage.save_file(config.allowed_file)
            update_message = f'La base de participants a ete mise a jour par {author.name}.'
            self.logger.info(update_message)
            await admin_channel.send(update_message)
        except Exception as e:
            await self.send_and_log(
                logging.ERROR, author,
                f'Erreur pendant la mise a jour: {str(e)}.')
