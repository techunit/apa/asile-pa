import logging

from discord.ext import commands
from features.custom_permissions import has_channel_command_permission
from services.app_data import AppData

from .base_cog import BaseCog


class FooterSetter(BaseCog):
    """ Command group handling download / upload """
    def __init__(self, bot: commands.Bot, app_data: AppData,
                 logger: logging.Logger):
        self.bot = bot
        self.app_data = app_data
        self.logger = logger

    @commands.command()
    @has_channel_command_permission()
    async def set_footer(self, ctx, message: str):
        """Update footer to send after task"""
        if len(message) > 1700:
            await self.send_and_log(
                logging.INFO, ctx.author,
                'La taille du message ne peut pas depasser les 1700 caractères.'
            )
            return
        self.app_data.config.message_footer = message

    @commands.command()
    @has_channel_command_permission()
    async def print_footer(self, ctx):
        await ctx.message.channel.send(self.app_data.config.message_footer)
