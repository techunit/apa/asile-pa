import logging
from dataclasses import dataclass

import discord
from discord.ext import commands
from services.app_data import AppData


@dataclass
class BaseCog(commands.Cog):
    """ Base cog with some utils """
    bot: commands.Bot
    app_data: AppData
    logger: logging.Logger

    def get_config(self):
        return self.app_data.config

    def get_storage(self):
        return self.app_data.storage

    # TODO: deduplicate
    async def get_guild(self) -> discord.Guild:
        # Find the correct guild.
        guild = discord.utils.get(self.bot.guilds,
                                  name=self.app_data.config.guild_name)
        if guild is None:
            logging.warning(
                f'Cannot find server with name: {self.app_data.config.guild_name}'
            )
        return guild

    async def send_and_log(self, level: int, author: discord.User, msg: str):
        self.logger.log(level, msg)
        await author.send(msg)
