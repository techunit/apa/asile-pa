import discord
from core.date_utils import now_as_str
from discord.ext import commands
from features.custom_permissions import has_channel_command_permission
from services.app_data import AppData
from services.dispatchers import DiscordDispatcher

from .base_cog import BaseCog


class StorageUtilsCog(BaseCog):
    """ Command group handling remove / adding tasks from storage, maybe more"""
    def __init__(self, bot: commands.Bot, app_data: AppData, logger):
        self.bot = bot
        self.app_data = app_data
        self.logger = logger

    @commands.command()
    @has_channel_command_permission()
    async def list_tasks(self, ctx):
        tasks = self.get_storage().tasks
        dispatcher = self.__create_dispatcher(ctx)
        if not tasks:
            await dispatcher.broadcast("Aucune tache charge!")
        else:
            message = "Nombre de taches:\n"
            for task_type in tasks:
                message += f"\t{task_type}: {len(tasks[task_type])}\n"

            await dispatcher.broadcast(message)

    @commands.command()
    @has_channel_command_permission()
    async def remove_tasks(self, ctx, *tasks_messages):
        dispatcher = self.__create_dispatcher(ctx)
        success = self.get_storage().remove_tasks(tasks_messages)
        if success:
            self.__save_file()
            msg = "Tache(s) retirées!"
        else:
            msg = "Erreur lors de la supression des taches"
        await dispatcher.broadcast(msg)

    @commands.command()
    @has_channel_command_permission()
    async def add_tasks(self, ctx, task_type: str, *tasks_messages):
        dispatcher = self.__create_dispatcher(ctx)
        # Just in case make sure the type already exists.
        known_types = self.get_storage().tasks.keys()
        if task_type not in known_types:
            await dispatcher.broadcast("Type inconnu")
            return

        success = self.get_storage().add_tasks(task_type, tasks_messages)
        if success:
            self.__save_file()
            msg = "Ajout effectué!"
        else:
            msg = "Erreur lors de l'ajout des taches"
        await dispatcher.broadcast(msg)

    def __save_file(self):
        storage = self.get_storage()
        config = self.get_config()
        formatted_date = f"{config.save_dir}{now_as_str()}.xlsx"
        storage.save_file(formatted_date)
        storage.save_file(config.allowed_file)

    def __create_dispatcher(self, ctx):
        admin_room = discord.utils.get(ctx.guild.channels,
                                       name=self.get_config().admin_room)
        return DiscordDispatcher(ctx.author, [admin_room], self.logger)
