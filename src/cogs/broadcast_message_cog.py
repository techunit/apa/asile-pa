import logging

from discord import TextChannel
from discord.ext import commands
from features.custom_permissions import has_channel_command_permission
from services.app_data import AppData
from services.dispatchers import DiscordDispatcher

from .base_cog import BaseCog


class BroadcastMessageCog(BaseCog):
    """ Command group handling download / upload """
    def __init__(self, bot: commands.Bot, app_data: AppData,
                 logger: logging.Logger):
        self.bot = bot
        self.app_data = app_data
        self.logger = logger

    @commands.command()
    @has_channel_command_permission()
    async def broadcast(self, ctx: commands.Context, send_to_rooms: bool,
                        message: str):
        """Update footer to send after task"""
        if len(message) > 1700:
            await self.send_and_log(
                logging.INFO, ctx.author,
                'La taille du message ne peut pas dépasser les 1700 caractères.'
            )
            return

        if send_to_rooms:
            rooms_prefix = self.get_config().worker_room_begin
            is_unit_channel = lambda c: isinstance( c, TextChannel)\
                                        and c.name.startswith(rooms_prefix)
            channels = list(filter(is_unit_channel, ctx.guild.channels))
        else:
            channels = [ctx.channel]

        if len(channels) < 1:
            self.logger.debug(
                'Trying to broadcast without any matching channel')

        dispatcher = DiscordDispatcher(ctx.author, channels, self.logger)
        await dispatcher.broadcast(message)
