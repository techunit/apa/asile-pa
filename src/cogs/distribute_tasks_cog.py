import asyncio
import logging
import typing
from datetime import datetime

import discord
from core.date_utils import next_datetime, now_as_str
from discord.ext import commands, tasks
from features.assign_task_command import AssignTaskCommand
from features.custom_permissions import has_dm_command_permission
from services.app_data import AppData
from services.dispatchers import DiscordDispatcher
from services.worker_fetcher import DiscordWorkerFetcher
from services.worker_reminder import WorkerReminder

from .base_cog import BaseCog


class DistributeTasksCog(BaseCog):
    """ Command group handling download / upload """
    def __init__(self, bot: commands.Bot, app_data: AppData,
                 logger: logging.Logger):
        self.bot = bot
        self.app_data = app_data
        self.logger = logger
        self.first_iteration_time = None
        self.dispatching_in_progress = False
        self.worker_reminder = WorkerReminder(
            self.app_data.config.assignement_pending_filepath,
            self.app_data.config.assignement_completed_filepath)

    @commands.command()
    @commands.dm_only()
    async def ok(self, ctx):
        dispatcher = DiscordDispatcher(ctx.author, [], self.logger)
        self.worker_reminder.complete_tasks_of_worker(ctx.author.id,
                                                      ctx.author.name,
                                                      datetime.now())
        if not self.dispatching_in_progress:
            self.worker_reminder.save()
        await dispatcher.send_message("Bien reçu 🎅")

    @commands.command()
    @has_dm_command_permission()
    async def distribute(self,
                         ctx,
                         max_task_per_worker: int,
                         dry_run: typing.Optional[bool] = True):
        self.dispatching_in_progress = True
        try:
            if dry_run is None:
                self.logger.warning(
                    'Cannot run distribute, dry_run value is None')
                return
            guild = await self.get_guild()
            await self.assign_tasks(ctx.author, guild, max_task_per_worker,
                                    dry_run)
        except Exception as excep:
            self.logger.error(f'Error while running distribute: {excep}')
        self.dispatching_in_progress = False

    @commands.Cog.listener()
    async def on_ready(self):
        """ Initialize the schedule assigmnent if enabled """
        config = self.app_data.config
        if not config.scheduling_enabled or \
            self.assigment_schedule.is_running():
            return

        guild = await self.get_guild()
        if guild is None:
            self.logger.error('Cannot schedule task as guild cannot be found')
        self.first_iteration_time = next_datetime(config.schedule_hour,
                                                  config.schedule_minute)
        self.assigment_schedule.start(None, guild,
                                      config.daily_task_per_worker)

    @tasks.loop(hours=24)
    async def assigment_schedule(self, author, guild: discord.Guild,
                                 max_task_per_worker):
        """ Schedules the assigment of the task. """
        try:
            self.logger.info(f'Running scheduled task at {datetime.now()}')
            await self.assign_tasks(author,
                                    guild,
                                    max_task_per_worker,
                                    dry_run=False)
        except Exception as err:
            self.logger.error("Error while running the assigment schedule")
            self.logger.exception(err)

    @assigment_schedule.before_loop
    async def before_assigment_schedule(self):
        """
          Ensures the first iteration of the task is running at the right
          time.
        """
        try:
            current_time = datetime.now()
            self.logger.info(f'Triggering at {current_time}')
            if current_time < self.first_iteration_time:
                delta_sec = (self.first_iteration_time -
                             current_time).total_seconds() - 20
                if delta_sec > 0:
                    self.logger.info(
                        'Will wait for {:.2f} minutes (~{:.2f} hours)'.format(
                            delta_sec / 60, delta_sec / 3600))
                    await asyncio.sleep(delta_sec)

            self.logger.info(
                f'First iteration of the task at {datetime.now()}')
        except Exception as err:
            self.logger.error("Error while scheduling assigment")
            self.logger.exception(err)

    async def assign_tasks(self,
                           author,
                           guild: discord.Guild,
                           max_task_per_worker=10,
                           dry_run=True):
        logger = logging.getLogger("asilepa")
        if max_task_per_worker < 1:
            logger.debug("Number of task must be strictly positive")
            return

        config = self.app_data.config
        storage = self.app_data.storage

        if storage is None:
            logger.exception("Storage is not initialized.")
            return

        admin_channel = discord.utils.get(guild.channels,
                                          name=config.admin_room)
        dispatcher = DiscordDispatcher(author, [admin_channel], logger)
        fetcher = DiscordWorkerFetcher(guild, storage,
                                       config.worker_role_begin)
        command = AssignTaskCommand(storage, dispatcher, fetcher,
                                    self.worker_reminder, max_task_per_worker,
                                    config.message_header,
                                    config.message_footer)

        active_workers, inactive_workers = await command.execute(dry_run)
        if not dry_run:
            formatted_date = f"{config.save_dir}{now_as_str()}.xlsx"
            storage.save_file(formatted_date)
            storage.save_file(config.allowed_file)
            self.worker_reminder.save()
            await self.remove_users(inactive_workers, dispatcher)
        else:
            storage.save_file(f"{config.save_dir}dry_{now_as_str()}.xlsx")

    async def remove_users(self, inactive_workers,
                           dispatcher: DiscordDispatcher):
        if not inactive_workers:
            return

        names = list(map(lambda w: w.name, inactive_workers))
        summary = 'Membre inactifs du jour sont ' + (', '.join(names))

        for worker in inactive_workers:
            try:
                self.logger.warning(f'{worker.name} est inactif')
                member = worker.member
                roles = list(
                    filter(
                        lambda r: r.name.startswith(self.app_data.config.
                                                    worker_role_begin),
                        member.roles))

                await member.remove_roles(*roles)
                await dispatcher.dispatch(worker, (
                    "Tu as été considéré comme inactif, "
                    "tu peux toujours nous rejoindre de nouveau en contactant les admins."
                ))
            except Exception as err:
                self.logger.error(f"Couldn't remove roles from {worker.name}")
                self.logger.exception(err)

        await dispatcher.broadcast(summary)
