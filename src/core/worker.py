from dataclasses import dataclass


@dataclass
class Worker:
    id: int
    name: str
    history: set()
    member: object = None


@dataclass
class Reminder:
    worker_id: int
    days_past: int
    tasks: set()


@dataclass
class Task:
    task_id: int
    task_message: str
    task_type: type
