from datetime import datetime, timedelta


def now_as_str() -> str:
    """ Returns the current time as string. """
    now = datetime.now()
    return now.strftime("%Y-%m-%d_%H-%M-%S")


def next_datetime(hour: int, minute: int) -> datetime:
    """ Returns the next datetime matching the given hour and minute. """
    now = datetime.now()
    t = datetime(now.year, now.month, now.day, hour, minute)
    if now.hour > hour or (now.hour == hour and now.minute > minute):
        t += timedelta(days=1)
    return t
