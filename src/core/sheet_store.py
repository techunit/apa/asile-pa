from dataclasses import dataclass
from io import BytesIO
from typing import Dict, List, Set

from openpyxl import Workbook, load_workbook

from .worker import Task


def is_data_cell(value):
    """Whether the cell as a non empty value."""
    return (value is not None) and value != ""


@dataclass
class Storage:
    tasks: Dict[str, Dict[int, str]]
    user_history: Dict[int, Set[int]]

    def __find_next_id(self):
        biggest_id = 0
        for filtred_tasks in self.tasks.values():
            biggest_id = max(biggest_id, max(filtred_tasks.keys()))
        for user_history in self.user_history.values():
            biggest_id = max(biggest_id, max(user_history))
        return biggest_id + 1

    def remove_tasks(self, tasks_messages: List[str]) -> bool:
        """ Removes task having message included in task_handle_to_remove. """
        for task_type in self.tasks.keys():
            message_to_ids = {v: k for k, v in self.tasks[task_type].items()}

            for message in tasks_messages:
                if message in message_to_ids:
                    task_id = message_to_ids[message]
                    del self.tasks[task_type][task_id]
        return True

    def add_tasks(self, task_type: str, tasks_messages: List[str]) -> bool:
        """ Adds new tasks with the given messages. """
        filtred_tasks = self.tasks[task_type]
        next_assigned_id = self.__find_next_id()
        for message in tasks_messages:
            if message not in filtred_tasks.values():
                filtred_tasks[next_assigned_id] = message
                next_assigned_id += 1
        return True


@dataclass
class SheetStore(Storage):
    """Implementation of Storage loading from Excel."""
    __workbook: Workbook
    __task_sheet_name: str
    __worker_history: str

    def remove_tasks(self, tasks_messages: List[str]) -> bool:
        """ Removes task having message included in task_handle_to_remove. """
        if super().remove_tasks(tasks_messages):
            self.__update_tasks()
            return True
        return False

    def add_tasks(self, task_type: str, tasks_messages: List[str]) -> bool:
        """ Adds new tasks with the given messages. """
        if super().add_tasks(task_type, tasks_messages):
            self.__update_tasks()
            return True
        return False

    def save(self) -> BytesIO:
        self.__update_workers()
        out = BytesIO()
        self.__workbook.save(out)
        out.seek(0)
        return out

    def save_file(self, filename):
        self.__update_workers()
        self.__workbook.save(filename)

    def __update_tasks(self):
        """ Update the tasks worker history"""
        temp_tasks = []
        for task_type, filtered_tasks in self.tasks.items():
            for task_id, task_message in filtered_tasks.items():
                temp_tasks.append(Task(task_id, task_message, task_type))

        tasksheet = self.__workbook[self.__task_sheet_name]
        tasksheet.delete_rows(1, tasksheet.max_row)
        for task in temp_tasks:
            content = [task.task_message, str(task.task_id), task.task_type]
            tasksheet.append(content)

    def __update_workers(self):
        """ Update the workbook worker history"""
        workers = self.__workbook[self.__worker_history]
        workers.delete_rows(1, workers.max_row)
        for user_id, history in self.user_history.items():
            if len(history) == 0:
                continue
            content = [str(user_id), ','.join(map(str, history))]
            workers.append(content)

    @staticmethod
    def load_data_from_bytes(content: bytes, tasks_sheet: str,
                             worker_history_sheet: str):
        return SheetStore.__load_data(BytesIO(content), tasks_sheet,
                                      worker_history_sheet)

    @staticmethod
    def load_data(filename: str, tasks_sheet: str, worker_history_sheet: str):
        return SheetStore.__load_data(filename, tasks_sheet,
                                      worker_history_sheet)

    @staticmethod
    def __load_data(file, tasks_sheet: str, worker_history_sheet: str):
        workbook = load_workbook(file)
        tasks = SheetStore.__extract_tasks(workbook, tasks_sheet)
        workers_history = \
            SheetStore.__extract_workers_history(workbook, worker_history_sheet)
        store = SheetStore(tasks, workers_history, workbook, tasks_sheet,
                           worker_history_sheet)
        return store

    @staticmethod
    def __extract_tasks(workbook: Workbook, sheet_name: str):
        tasks_sheet = workbook[sheet_name]
        tasks = {}
        for row in tasks_sheet.rows:
            try:
                handle = row[0].value
                task_id = row[1].value
                task_type = row[2].value
                if handle != "" and task_id != "":
                    if task_type not in tasks:
                        tasks[task_type] = {}
                    tasks[task_type][task_id] = handle
            except Exception as err:
                raise Exception(
                    "While extracting task, task_id or message most likely was invalid/null("
                    + str(err) + ')')

        return tasks

    @staticmethod
    def __extract_workers_history(workbook: Workbook, sheet_name: str):
        workers_history = {}
        workers_history_sheet = workbook[sheet_name]
        for row in workers_history_sheet.rows:
            if is_data_cell(row[0].value) and is_data_cell(row[1].value):
                worker_id = int(row[0].value)
                task_history = row[1].value
                if isinstance(task_history, str):
                    workers_history[worker_id] = set(
                        map(int, task_history.split(',')))
                else:
                    workers_history[worker_id] = set([task_history])
        return workers_history
