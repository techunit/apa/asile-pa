import os
from pathlib import Path

from dotenv import load_dotenv


class BotConfig:
    def __init__(self):
        load_dotenv(verbose=True)

        try:
            self.token = self._get_value('DISCORD_BOTTOKEN')
            self.guild_name = self._get_value('DISCORD_SERVERNAME')
            self.admin_room = self._get_value('DISCORD_ADMIN_ROOM_NAME')
            self.organizer_role = self._get_value(
                'DISCORD_ORGANIZER_ROLE_NAME')
            self.worker_role_begin = self._get_value(
                'DISCORD_WORKER_ROLE_PREFIX')
            self.worker_room_begin = self._get_value(
                'DISCORD_WORKER_ROOM_PREFIX')
            self.daily_task_per_worker = int(
                self._get_value('ASSIGNMENT_DAILY_TASK_PER_WORKER'))

            self.backup_dir = self._get_value('DATAFILE_BACKUP_DIRECTORY')
            # Make sure the directory exists
            Path(self.backup_dir).mkdir(parents=True, exist_ok=True)
            self.save_dir = self._sanitize_path(f'{self.backup_dir}{self.guild_name}/')
            self.upload_dir = self._sanitize_path(f'{self.save_dir}/uploads/')
            Path(self.upload_dir).mkdir(parents=True, exist_ok=True)

            self.allowed_file = self._get_value('DATAFILE_NAME')
            self.max_file_size = int(self._get_value('DATAFILE_MAX_SIZE'))
            self.task_sheet = self._get_value('DATAFILE_TASKSHEET_NAME')
            self.worker_history_sheet = self._get_value(
                'DATAFILE_HISTORYSHEET_NAME')
            self.assignement_pending_filepath = self._get_value('ASSIGNMENT_PENDING_FILEPATH')
            self.assignement_completed_filepath = self._get_value('ASSIGNMENT_COMPLETED_FILEPATH')

            self.scheduling_enabled = self._get_value(
                'ASSIGNMENT_SCHEDULING_ENABLED') == 'True'
            self.schedule_hour = int(
                self._get_value('ASSIGNMENT_SCHEDULE_HOUR'))
            self.schedule_minute = int(
                self._get_value('ASSIGNMENT_SCHEDULE_MINUTE'))

            self.message_header = self._get_value('ASSIGNMENT_MESSAGE_HEADER')
            self.message_footer = self._get_value('ASSIGNMENT_MESSAGE_FOOTER')

        except Exception as exception:
            raise ValueError("Invalid configuration") from exception

    @staticmethod
    def _get_value(var_name):
        value = os.getenv(var_name)
        assert (value is not None
                and value != ''), f'{var_name} was not configured'
        return value
    
    @staticmethod
    def _sanitize_path(var_path):
        if var_path:
            return "".join(x for x in var_path if x.isalnum() or x == '/')
        else:
            return ""

