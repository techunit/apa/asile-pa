import logging
import traceback

# from dotenv import load_dotenv
import discord
from discord.ext import commands

import cogs
from cogs.distribute_tasks_cog import DistributeTasksCog
from cogs.file_cog import FileCog
from cogs.footer_setter_cog import FooterSetter
from cogs.storage_utils_cog import StorageUtilsCog
from services.app_data import AppData

LOG_STRING_FORMAT = '%(asctime)s:%(levelname)s:%(name)s: %(message)s'
logger = logging.getLogger('asilepa')
logging.basicConfig(format=LOG_STRING_FORMAT, level=logging.INFO)
handler = logging.FileHandler(filename='asilepa.log',
                              encoding='utf-8',
                              mode='a')
handler.setFormatter(logging.Formatter(LOG_STRING_FORMAT))
logger.addHandler(handler)

try:
    app = AppData.get_instance()
    config = app.config
except Exception as e:
    logger.exception("Invalid configuration did you updated your .env")
    exit()

logger.info("Bot starting...")

intents = discord.Intents.default()
intents.members = True

bot = commands.Bot(command_prefix='!', intents=intents)


async def on_ready():
    logger.info('%s has connected to Discord!', bot.user)


@bot.event
async def on_command(ctx):
    logger.info(f"Command sent by ({ctx.author})\n [{ctx.message.content}]\n")


@bot.event
async def on_command_error(event, exception):
    exception_str = ''.join(
        traceback.format_exception(type(exception), exception,
                                   exception.__traceback__))
    logger.error(
        f"Error on command by ({event.author}) [{str(exception)}]\n{exception_str}\n"
    )


try:
    bot.add_cog(cogs.BroadcastMessageCog(bot, app, logger))
    bot.add_cog(DistributeTasksCog(bot, app, logger))
    bot.add_cog(FileCog(bot, app, logger))
    bot.add_cog(FooterSetter(bot, app, logger))
    bot.add_cog(StorageUtilsCog(bot, app, logger))
    bot.run(config.token)
except Exception as excpt:
    logger.error(str(excpt))

logger.info("Bot terminating")
