from dataclasses import dataclass
from typing import List

from core.sheet_store import Storage
from core.worker import Worker
from discord import Guild


class WorkerFetcher:
    def fetch_all(self) -> List[Worker]:
        pass

class CachedWorkerFetcher(WorkerFetcher):
    """ Worker fetcher caching a delate """
    def __init__(self, delagated_fetcher: WorkerFetcher):
        self.__fetcher = delagated_fetcher
        self.__members = None

    def fetch_all(self) -> List[Worker]:
        if self.__members is None:
            self.__members = self.__fetcher.fetch_all()
        return self.__members


@dataclass
class DiscordWorkerFetcher(WorkerFetcher):
    guild: Guild
    storage: Storage
    work_role_pattern: str

    def fetch_all(self):
        """Returns list of workers in the Guild."""
        units = {}
        members = set()

        for member in self.guild.members:
            if member.bot:
                continue

            for role in member.roles:
                if role.name.startswith(self.work_role_pattern):
                    if units.get(role.name) is None:
                        units[role.name] = set()
                    units[role.name].add(member.id)
                    members.add(member)
                    break

        workers = []
        for m in members:
            history = set()
            if m.id in self.storage.user_history:
                history = self.storage.user_history[m.id]
            workers.append(
                Worker(m.id, m.nick if m.nick else m.name, history, m))
        return workers, units
