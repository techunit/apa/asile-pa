from core.bot_config import BotConfig
from core.sheet_store import SheetStore


class AppData:
    __instance = None

    @staticmethod
    def get_instance():
        """Get the globally shared instance"""
        if AppData.__instance is None:
            AppData()
        return AppData.__instance

    def __init__(self):
        """ This constructor should not be directly called. """
        if AppData.__instance is not None:
            raise Exception("App data is already instantiated")
        else:
            # Load initial data
            self.config = BotConfig()
            self.storage = SheetStore.load_data(
                self.config.allowed_file, self.config.task_sheet,
                self.config.worker_history_sheet)
            AppData.__instance = self
