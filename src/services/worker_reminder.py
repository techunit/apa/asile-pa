import json
from datetime import datetime
from pathlib import Path

from core.worker import Reminder


class WorkerReminder:
    max_remaining_days = 7
    workers_pending_assignments = dict()
    workers_completed_assignments = dict()
    assignement_pending_filepath: str
    assignement_completed_filepath: str

    def __init__(self, assignement_pending_filepath,
                 assignement_completed_filepath):
        self.assignement_pending_filepath = assignement_pending_filepath
        self.assignement_completed_filepath = assignement_completed_filepath
        if Path(assignement_pending_filepath).exists():
            with open(assignement_pending_filepath) as assignements_cache_file:
                self.workers_pending_assignments = json.load(
                    assignements_cache_file)
                assignements_cache_file.close()
        if Path(assignement_completed_filepath).exists():
            with open(
                    assignement_completed_filepath) as assignements_cache_file:
                self.workers_completed_assignments = json.load(
                    assignements_cache_file)
                assignements_cache_file.close()

    def get_pending_tasks_of_worker(self, worker_id, fromDate):
        """ Get pending tasks for the given worker id ."""
        worker_id = str(worker_id)
        if worker_id in self.workers_pending_assignments:
            worker_state = self.workers_pending_assignments[worker_id]
            worker_tasks = worker_state["tasks"]
            if len(worker_tasks) > 0:
                date = datetime.strptime(
                    worker_state["oldest_assignement_date"], '%Y-%m-%d')
                return Reminder(worker_id, (fromDate - date).days,
                                worker_tasks)

    def complete_tasks_of_worker(self, worker_id, worker_name, at_date):
        worker_id = str(worker_id)
        date_str = at_date.strftime("%Y-%m-%d %H:%M")
        if worker_id in self.workers_pending_assignments:
            pending_assignements = self.workers_pending_assignments[worker_id]
            if worker_id not in self.workers_completed_assignments:
                self.workers_completed_assignments[worker_id] = {}
            self.workers_completed_assignments[worker_id][date_str] = \
                pending_assignements["tasks"]
            self.workers_completed_assignments[worker_id]["name"] = worker_name
            del self.workers_pending_assignments[worker_id]

    def add_pending_tasks_to_worker(self, worker_id, date,
                                    task_assigment_per_type):
        worker_id = str(worker_id)
        for assigment in task_assigment_per_type.values():
            handles = assigment.values()
            if worker_id in self.workers_pending_assignments:
                self.workers_pending_assignments[worker_id]["tasks"] = list(
                    set(self.workers_pending_assignments[worker_id]["tasks"])
                    | set(handles))
            else:
                self.workers_pending_assignments[worker_id] = {
                    "oldest_assignement_date": date.strftime("%Y-%m-%d"),
                    "tasks": list(handles)
                }

    def save(self,
             assignement_pending_filepath=None,
             assignement_completed_filepath=None):
        if not assignement_pending_filepath:
            assignement_pending_filepath = self.assignement_pending_filepath
        if not assignement_completed_filepath:
            assignement_completed_filepath = self.assignement_completed_filepath

        with open(assignement_pending_filepath, 'w+') as jsonfile:
            json.dump(self.workers_pending_assignments, jsonfile)
        with open(assignement_completed_filepath, 'w+') as jsonfile:
            json.dump(self.workers_completed_assignments, jsonfile)

    def get_reminder_message(self, worker, at_date):
        reminder = self.get_pending_tasks_of_worker(worker.id, at_date)
        if reminder and reminder.days_past > 0:
            return f'Rappel, il vous reste {self.max_remaining_days - reminder.days_past} jours pour confirmer l\'envoi de votre message à {", ".join(reminder.tasks)}'

    def get_inactive_workers_ids(self, fromDate):
        inactive_worker = list()
        for worker_id in self.workers_pending_assignments.keys():
            worker_state = self.workers_pending_assignments[worker_id]
            date = datetime.strptime(worker_state["oldest_assignement_date"],
                                     '%Y-%m-%d')
            if ((fromDate - date).days) > self.max_remaining_days:
                inactive_worker.append(int(worker_id))
        return inactive_worker
