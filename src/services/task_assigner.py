import random
import time
from dataclasses import dataclass

from munkres import Munkres


@dataclass
class SubWorker:
    key: int
    occurence: int


# Assign task to discord member.
class TaskAssigner:
    def __init__(self, workers, tasks, worker_max_task_per_iteration=10):
        self.workers = workers
        self.tasks = tasks
        self.worker_iteration_limit = worker_max_task_per_iteration

    # The list of subworker - task
    def get_subworker_tasks(self):
        # We will deduplicate workers by number of task they can achieve.
        sub_worker_incr = 0
        pending_workers = {}
        doable_tasks = set()
        for worker in self.workers:
            remaining_tasks = self.tasks.keys() - worker.history
            allowed_repetition = min(len(remaining_tasks),
                                     self.worker_iteration_limit)
            for i in range(allowed_repetition):
                pending_workers[sub_worker_incr] = SubWorker(worker.id, i + 1)
                sub_worker_incr += 1
            doable_tasks = doable_tasks | remaining_tasks

        return doable_tasks, pending_workers

    def resolve(self):
        # We will deduplicate workers by number of task they can achieve.
        doable_tasks, pending_workers = self.get_subworker_tasks()

        # We will to have the task indexed for the matrix later.
        indexed_tasks = list(doable_tasks)
        # Shuffle lists to avoid potential preferences due to order.
        random.shuffle(indexed_tasks)
        random.shuffle(pending_workers)

        tasks_ids_to_pos = {}
        for i in range(len(indexed_tasks)):
            tasks_ids_to_pos[indexed_tasks[i]] = i

        # For each worker, build indices of the subset of task to discard.
        removable_tasks_pos = {}
        for worker in self.workers:
            removable_tasks_pos[worker.id] = []
            for t in worker.history:
                pos = tasks_ids_to_pos.get(t)
                if pos:
                    removable_tasks_pos[worker.id].append(pos)

        # create the matrix.
        matrix = []
        for i in range(len(pending_workers)):
            # Create slight preference to distribute accross worker.
            subworker = pending_workers[i]
            cost = 1 + (subworker.occurence - 1) / 100
            row = [cost] * len(doable_tasks)
            for idx in removable_tasks_pos[subworker.key]:
                row[idx] = 10000
            matrix.append(row)

        solver = Munkres()
        print("solver start")
        beg = time.time()
        result = solver.compute(matrix)
        end = time.time()
        print(f'solver end: {end - beg}')

        user_assignment = {}
        assigned_tasks = 0
        for row, col in result:
            # If the value is high we don't want to pair.
            if col > len(indexed_tasks) or matrix[row][col] > 9999:
                continue

            worker_key = pending_workers[row].key
            if worker_key not in user_assignment:
                user_assignment[worker_key] = {}
            task_id = indexed_tasks[col]
            user_assignment[worker_key][task_id] = self.tasks[task_id]
            assigned_tasks += 1

        return user_assignment, assigned_tasks
