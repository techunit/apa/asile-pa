import asyncio
import logging
import typing
from dataclasses import dataclass

from core.worker import Worker
from discord import HTTPException, TextChannel, User, VoiceChannel

# Log about dispatch request
dispatch_logger = logging.getLogger('dispatcher')
dispatch_logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='dispatcher.log',
                              encoding='utf-8',
                              mode='a')
handler.setFormatter(
    logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
dispatch_logger.addHandler(handler)


class DispatcherInterface:
    async def broadcast(self, message):
        pass

    async def send_message(self, message):
        pass

    async def dispatch(self, worker, message) -> bool:
        pass


@dataclass
class StdoutDispatcher(DispatcherInterface):
    async def broadcast(self, message):
        print(message)

    async def send_message(self, message):
        print(f'Sent:\n{message}')

    async def dispatch(self, worker, message) -> bool:
        print(f'To {worker.name}:\n{message}')
        return True


@dataclass
class DiscordDispatcher(DispatcherInterface):
    requester: User
    channels: typing.List[TextChannel]
    logger: logging.Logger

    async def broadcast(self, message):
        try:
            dispatch_logger.info("broadcast")
            for c in self.channels:
                if isinstance(c, VoiceChannel):
                    dispatch_logger.warning("Sending message to voice channel")
                    continue

                await self.__send_to_open_channel(c, message)
                if len(self.channels) > 1:
                    await asyncio.sleep(1.8)
            return True
        except Exception as e:
            channel_names = ','.join(list(map(lambda c: c.name,
                                              self.channels)))
            self.logger.error(
                f"Error while trying to broadcast in {channel_names},\n message[{message}]"
            )
            self.logger.error(e)
            return False

    async def send_message(self, message):
        if self.requester:
            await self.__send_dm(self.requester, self.requester.name, message)

    async def dispatch(self, worker: Worker, message) -> bool:
        return await self.__send_dm(worker.member, worker.name, message)

    async def __send_dm(self, user, name, message) -> bool:
        try:
            dispatch_logger.info(f"Sending {message} to {name}")
            await user.create_dm()
            await self.__send_to_open_channel(user.dm_channel, message)
            return True
        except HTTPException as e:
            self.logger.error(
                f"Error dm-ing {name}.\n Exception:\n{str(e)}\nMessage\n{message}\n]"
            )
            return False

    async def __send_to_open_channel(self, destination_channel, message):
        # We limit the number of char we send in one
        # go
        buffer = ''
        lines = message.splitlines()
        for l in lines:
            if len(buffer) + len(l) > 1700:
                await destination_channel.send(buffer)
                buffer = ''
            buffer += l + '\n'
        # if we have remaining content send i
        if len(buffer) > 0:
            await destination_channel.send(buffer)
