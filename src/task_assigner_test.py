import asyncio
import os
from datetime import datetime

from dotenv import load_dotenv

from core.sheet_store import SheetStore
from core.worker import Worker
from features.task_distributor import TaskDistributor
from services.worker_reminder import WorkerReminder
from services.dispatchers import StdoutDispatcher


def save_file(filename: str, data):
    backup_file = os.open(filename, os.O_CREAT | os.O_WRONLY)
    os.write(backup_file, data.getvalue())
    os.close(backup_file)


async def test():
    worker_reminder = WorkerReminder(os.getenv('ASSIGNMENT_PENDING_FILEPATH'), os.getenv('ASSIGNMENT_COMPLETED_FILEPATH'))
    store = SheetStore.load_data(os.getenv('ALLOWED_FILE'),
                                 os.getenv('TASK_SHEET'),
                                 os.getenv('WORKER_HISTORY_SHEET'))
    workers = []
    test_user_id = 7357
    workers.append(Worker(test_user_id, str(test_user_id), set()))
    for i in range(1, 12):
        previous_tasks = {2 * i}
        if i % 2 == 0:
            for j in range(10, 200):
                previous_tasks.add(j)
        workers.append(Worker(i, str(i), previous_tasks))


    distributor = TaskDistributor(store.tasks, StdoutDispatcher(), worker_reminder)
    await distributor.distribute(workers, dry_run=True)

    now = datetime.now()
    formatted_date = now.strftime("data/tests/%Y-%m-%d_%H-%M-%S.xlsx")
    data = store.save()
    save_file(formatted_date, data)
    save_file("test.xlsx", data)
    worker_reminder.save("test_cache_assignements_pending.json", "test_cache_assignements_completed.json")


load_dotenv()
asyncio.run(test())
