from dataclasses import dataclass

from core.sheet_store import Storage
from services.dispatchers import DispatcherInterface
from services.worker_fetcher import WorkerFetcher
from services.worker_reminder import WorkerReminder

from features.task_distributor import TaskDistributor


@dataclass
class AssignTaskCommand:
    storage: Storage
    dispatcher: DispatcherInterface
    fetcher: WorkerFetcher
    reminder: WorkerReminder
    max_task_per_worker: int
    message_header: str
    message_footer: str

    async def execute(self, dry_run=True):
        workers, _ = self.fetcher.fetch_all()
        if len(workers) == 0:
            await self.dispatcher.send_message('Aucun membre trouve')
            return [], []

        distributor = TaskDistributor(self.storage.tasks, self.dispatcher,
                                      self.reminder, self.message_header,
                                      self.message_footer)
        active_workers, inactive_workers = await distributor.distribute(
            workers, self.max_task_per_worker, dry_run)
        if not dry_run:
            for m in active_workers:
                if m.id in self.storage.user_history:
                    self.storage.user_history[m.id].update(m.history)
                else:
                    self.storage.user_history[m.id] = m.history
            await self.dispatcher.send_message('Fin du calcul')

        print("End of distribute")
        return active_workers, inactive_workers
