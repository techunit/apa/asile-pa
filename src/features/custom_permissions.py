import logging

import discord
from core.bot_config import BotConfig
from discord.ext import commands
from services.app_data import AppData


# TODO: deduplicate
def get_guild(bot: commands.Bot, config: BotConfig) -> discord.Guild:
    # Find the correct guild.
    guild = discord.utils.get(bot.guilds, name=config.guild_name)
    if guild is None:
        logging.warning(f'Cannot find server with name: {config.guild_name}')
    return guild


def is_organizer(bot: commands.Bot, user) -> bool:
    """ Whether the user is an organizer of the given guild """
    app_data = AppData.get_instance()
    guild = get_guild(bot, app_data.config)
    if guild is None:
        return False

    all_channels = guild.channels
    any_channel = all_channels[0] if all_channels else None
    if any_channel is None:
        return False

    member = guild.get_member(user.id)
    if member is None:
        return False

    role = discord.utils.get(member.roles, name=app_data.config.organizer_role)
    if role is None:
        return False

    permissions = any_channel.permissions_for(member)
    return permissions.administrator


def has_dm_command_permission():
    """ Whether the user has right to send commands to the bot via private message"""
    def predicate(ctx):
        return isinstance(ctx.channel, discord.DMChannel) \
               and is_organizer(ctx.bot, ctx.author)

    return commands.check(predicate)


def has_channel_command_permission():
    """ Whether the user has right to send commands to the bot"""
    def predicate(ctx):
        config = AppData.get_instance().config
        return ctx.guild is not None \
               and ctx.guild.name == config.guild_name \
               and ctx.channel is not None \
               and ctx.channel.name == config.admin_room \
               and is_organizer(ctx.bot, ctx.author)

    return commands.check(predicate)
