import asyncio
from dataclasses import dataclass
from datetime import datetime
from typing import Dict

from services.dispatchers import DispatcherInterface
from services.task_assigner import TaskAssigner
from services.worker_reminder import WorkerReminder


@dataclass
class TaskDistributor:
    """Class for a command distributing tasks."""
    tasks: dict
    dispatcher: DispatcherInterface
    worker_reminder: WorkerReminder
    message_header: str = ""
    message_footer: str = ""
    current_date = datetime.now()

    # Try to distribute as many task as possible to workers.
    # And update their history list if change have been made.
    async def distribute(self,
                         workers: list,
                         max_tasks_per_worker: int = 10,
                         dry_run: bool = True):
        await self.dispatcher.send_message('Calcul de la distribution...')
        inactive_workers_ids = self.worker_reminder.get_inactive_workers_ids(
            self.current_date)

        active_workers = list()
        inactive_workers = list()
        for worker in workers:
            if worker.id in inactive_workers_ids:
                inactive_workers.append(worker)
            else:
                active_workers.append(worker)

        if len(active_workers) == 0:
            await self.dispatcher.send_message("Aucun utilisateur actif")
            return active_workers, inactive_workers

        summary_text = ""
        if dry_run:
            summary_text = "**Dry run**\n"

        assignments_per_type = {}
        assigned_tasks_count_per_type = {}
        for key in self.tasks.keys():
            assigner = TaskAssigner(active_workers, self.tasks[key],
                                    max_tasks_per_worker)
            assignments, assigned_tasks_count = assigner.resolve()
            assigned_tasks_count_per_type[key] = assigned_tasks_count
            assignments_per_type[key] = assignments
            summary_text += (
                f'{assigned_tasks_count}/{len(self.tasks[key])} {key}s '
                f'ont ete assignes à {len(assignments)} membres.\n\n')

        # Due to the rate limit check if it makes sense to wait between active_workers.
        # The limit is at 120 events per minute, to make sure we don't it the limit
        # start waiting 1.8 seconds as soon as we have more than 40 member
        # (lets be extra safe)
        should_throttle = len(active_workers) > 40

        unreachable_recipients = []
        active_workers.sort(key=lambda w: w.name)
        for worker in active_workers:
            worker_assignments = TaskDistributor.__get_worker_asssigments(
                worker.id, assignments_per_type)
            if not worker_assignments:
                continue

            summary_text += (
                self.__worker_short_message(worker.name, worker_assignments) +
                '\n')
            if dry_run is False:
                if not await self.__dispatch(worker, worker_assignments):
                    unreachable_recipients.append(worker.name)

                if should_throttle:
                    await asyncio.sleep(1.8)
                for worker_tasks in worker_assignments.values():
                    worker.history = worker.history | set(worker_tasks.keys())
                self.worker_reminder.add_pending_tasks_to_worker(
                    worker.id, self.current_date, worker_assignments)

        if unreachable_recipients:
            summary_text += (
                f"\n\n Certains membres n'ont pas pu etre contacte: "
                f"{','.join(unreachable_recipients)}")

        if dry_run:
            await self.dispatcher.send_message(summary_text)
        else:
            await self.dispatcher.broadcast(summary_text)

        return active_workers, inactive_workers

    async def __dispatch(self, worker,
                         assigment_per_type: Dict[str, Dict[int, str]]):
        message = self.__dispatch_message(assigment_per_type)
        reminding_message = self.worker_reminder.get_reminder_message(
            worker, self.current_date)
        if reminding_message:
            message += f'\n{reminding_message}'
        return await self.dispatcher.dispatch(worker, message)

    def __dispatch_message(
            self, assigment_per_type: Dict[str, Dict[int, str]]) -> str:
        message = self.message_header
        for task_type, worker_tasks in assigment_per_type.items():
            adresses = worker_tasks.values()
            message += f"\n{task_type.lower()}: {', '.join(adresses)}"

        message += self.message_footer
        return message

    def __worker_short_message(
            self, worker_name,
            assigments_per_type: Dict[str, Dict[int, str]]) -> str:
        assigment_summary = ''
        for task_type in assigments_per_type:
            handles = assigments_per_type[task_type].values()
            assigment_summary += f'{task_type}(' + (', '.join(handles)) + ') '

        return f'{worker_name}: ' + assigment_summary

    @staticmethod
    def __get_worker_asssigments(worker_id, assigment_per_type):
        """ Returns map of type -> assigment for the given worker_id. """
        worker_assignments = {}
        for task_type, assigments in assigment_per_type.items():
            if worker_id in assigments:
                worker_assignments[task_type] = assigments[worker_id]
        return worker_assignments
