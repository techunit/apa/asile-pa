A discord bot assigning tasks(message) to members of a Discord server.

## Setup

Make sure the .env is configured with your bot/server information.

- Create a virtualenv: python3 -m venv ./venv
- Activate it: source ./venv/bin/activate
- Install requirements pip install -r requirements.txt

# Setup your bot

- TODO Link to external website about creating a bot.
- Make sure your bot has members intent permissions.

## How to use

Run bot.py.

All the commands have to be send to the bot as direct message.

- `!distribute [MAX_TASKS_PER_MEMBER] [safe_guard]`:  
  This distribute at most MAX_TASKS_PER_MEMBER per member.
  Member will each receive a direct message with their task.
  By default `safe_guard` is on, to send direct message add "off"
- `!download`:  
  Requests the updated excel file to the bot.
- Attachment with `!upload` as a comment:  
  Update the state of the application with the given excel file.
  Note: The filename must match the name provided in .env.
- `broadcast send_to_channel "message":`
  Sends the message to all the worker's room (see .env).
  send_to_channel can be `off` or `on`.

## Data file format (Excel)

The input file has to be a .xlsx file and should contain at least two sheets.

- One sheet should be named "Messages" with no header, and the first two columns should be:
  1. First column should contain the message, to distribute to the members.
  2. Second column is an identifier for each message (integer)
- The second sheet could be called "Workers history", and have two columns too. It keeps a list of messages previously
  distributed to members. If you are creating the file for the first time, leave it blank. If you are loading backup, do
  not edit the sheet manually.

## Links

- [discord.py documentation](https://discordpy.readthedocs.io/en/latest/index.html)
